$(".full-loader").show();
function datatable() {       
      $('.js-basic-table').DataTable()       
}
function loaderOpen(){
    $("body").css('overflow', 'hidden');
    $(".full-loader").show();
}
function loaderClose(){
    $("body").css('overflow', 'scroll');
    $(".full-loader").hide();
}
$(function () {
    $(".full-loader").hide();
    $('#dob').datepicker({
        format: 'dd-mm-yyyy',
        startDate: "-80y",
        endDate: '+1m',
        autoclose: true
     });
     datatable();
});

