angular.module('app', ['ngStorage'])
  .controller('logincontroller', function ($scope, $http, $localStorage) {
    function loadvar(){
    $scope.user = {};
    $scope.user.fname = "";
    $scope.user.lname = "";
    $scope.user.fathername = "";
    $scope.user.dob = "";
    $scope.user.gender = "Male";
    $scope.user.maritalstatus = "";
    $scope.user.religion = "";
    $scope.user.nationality = "";
    $scope.user.mno = "";
    $scope.user.email = "";
    $scope.user.address = "";
    $scope.user.url = "";
    }
    loadvar()
    $scope.temp = [];

    $scope.temp = $localStorage.prevPageData;
    $localStorage.prevPageData = [];
    $localStorage.prevPageData = $scope.temp;

    // function to submit the form after all validation has occurred 
    $scope.submitForm = function (isValid) {
      // check to make sure the form is completely valid
      if (isValid) {
        $scope.reguser();
        
      }
    };


    $scope.reguser = function () {
      loaderOpen()
      $localStorage.prevPageData.push($scope.user);
      $localStorage.prevPageData = $scope.temp;
      $('#registForm').trigger("reset");
      toastr.success('User Registered Successfully')
      loaderClose()
    }
  })
  .controller('viewcontroller', function ($scope, $http, $localStorage) {
    $scope.user = {};
    $scope.user.fname = "";
    $scope.user.lname = "";
    $scope.user.fathername = "";
    $scope.user.dob = "";
    $scope.user.gender = "Male";
    $scope.user.maritalstatus = "";
    $scope.user.religion = "";
    $scope.user.nationality = "";
    $scope.user.mno = "";
    $scope.user.email = "";
    $scope.user.address = "";
    $scope.user.url = "";

    $scope.userdata = $localStorage.prevPageData;

    $scope.viewdata = "";
    $scope.viewuser = function (data) {
      $scope.viewdata = data;
      $("#viewModal").modal('show');
    }
    // function to submit the form after all validation has occurred 
    $scope.submitForm = function (isValid) {
      // check to make sure the form is completely valid
      if (isValid) {
        $scope.upduser();
      }
    };
    $scope.updindex = "";
    $scope.edituser = function (data, index) {
      $scope.updindex = index;
      $scope.user.fname = data.fname;
      $scope.user.lname = data.lname;
      $scope.user.fathername = data.fathername;
      $scope.user.dob = data.dob;
      $scope.user.gender = data.gender;
      $scope.user.maritalstatus = data.maritalstatus;
      $scope.user.religion = data.religion;
      $scope.user.nationality = data.nationality;
      $scope.user.mno = data.mno;
      $scope.user.email = data.email;
      $scope.user.address = data.address;
      $scope.user.url = data.url;

      $("#editModal").modal('show');
    };

    $scope.upduser = function () {
      loaderOpen()
      $scope.userdata[$scope.updindex] = $scope.user;
      $localStorage.prevPageData = $scope.userdata;
      $("#editModal").modal('hide');
      toastr.success('User Updated Successfully')    
      loaderClose()  
    }

    $scope.clear = function () {
      loaderOpen()
      $localStorage.prevPageData = [];
      $scope.userdata =   $localStorage.prevPageData;
      if ( $.fn.DataTable.isDataTable('.js-basic-table')) {
        $('.js-basic-table').DataTable().destroy();
      }   
      setTimeout(function(){ 
        datatable();
        loaderClose();
       }, 2000);                 
    }

  })

